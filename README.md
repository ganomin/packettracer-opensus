# Install Cisco Packet Tracer on Fedora

Easily install Cisco Packet Tracer on openSUSE Tumbleweed (recent Leap version should be compatible too!). This installation script automatically detects a Cisco Packet Tracer `.deb` in any directory on `/home`.

On time writing this script current version in 8.1.1, this script may not support new version in future!

## Supported Fedora versions

- none, it's for openSUSE :P
- thanks to https://github.com/thiagojack/PacketTracer-Fedora for this script!

## Install

Follow these steps to easily install:

-   Download the Packet Tracer installer from [Cisco's download page](https://www.netacad.com/portal/node/488) and save it in any directory on `/home` using the default filename.
-   Clone this repo to your system
-   `cd` into the cloned repo
-   Run `sudo bash install.sh` and relax ;D

## Uninstall

-   If you need to uninstall, make sure the uninstall script is executable: `chmod +x uninstall.sh`
-   Run `sudo bash uninstall.sh`
